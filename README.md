# Overview

I wrote a sample code to see how cross-site cookies (with SameSite = None) work.

In PHP (or any other programming languages that exist), I believe the best way to set a cookie is using the `Set-Cookie` header which gives us the most flexibility compared to using already defined functions and classes.

[Set-Cookie at the Mozilla Network Documentation](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Set-Cookie)
