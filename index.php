<!--
    This page can be loaded from differnt domains to see if the cross-cookie works properly
    - https://random.works/CookieSync/index.php
    - https://random-works.local/CookieSync/index.php
    - https://apps.random.works/CookieSync/index.php
-->
<!DOCTYPE>
<html>
    <head>
        <meta charset="utf-8">
        <title>Cookie Sync</title>
    </head>
    <body>
        <img src="https://random-works.local/CookieSync/pixel.php">
        <script src="https://random-works.local/CookieSync/pixel.js.php"></script>
    </body>
</html>